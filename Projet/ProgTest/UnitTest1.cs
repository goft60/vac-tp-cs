using System;
using Xunit;

using ProgConsole;

namespace ProgTest
{
    public class UnitTest1
    {
        [Fact]
        public void Startsentence()
        {
            Assert.Equal(0, Program.FirstChar("blabla"));
            Assert.Equal(1, Program.FirstChar("Blabla."));
            Assert.Equal(-1, Program.FirstChar("é"));
        }
        [Fact]
        public void Endsentence()
        {
            Assert.Equal(0, Program.LastChar("blabla"));
            Assert.Equal(1, Program.LastChar("Blabla."));
        }
    }
}
