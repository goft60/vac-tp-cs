﻿using System;

namespace ProgConsole
{
    public class Program
    {
        public static int FirstChar(string sentence)
        {
            int caract=-1;
            if(sentence[0]>='A'&& sentence[0]<='Z')
                caract = 1;
            else if(sentence[0]>='a'&& sentence[0]<='z')
                caract = 0;
            return caract;
        }
        public static int LastChar(string sentence)
        {
            int caract=0;
            if(sentence[sentence.Length-1]=='.')
                caract=1;
            return caract;
        }
        static void Main(string[] args)
        {
            string sentence, final_sentence;
            int longueur_chaine = 0;
            do
            {
                Console.WriteLine("Saississez une phrase : ");
                sentence = Console.ReadLine();
                longueur_chaine = sentence.Length;
            }
            while(longueur_chaine < 2);
            final_sentence = String.Format("Vous avez saisi : \n\r {0} ",sentence);
            if(sentence[0]>='A' && sentence[0]<='Z')
                final_sentence = String.Format("{0}\n\r Cette phrase commence par une majucule.",final_sentence);
            else
                final_sentence = String.Format("{0}\n\r Cette phrase ne commence pas par une majuscule.",final_sentence);
            if(sentence[longueur_chaine - 1]=='.')
                final_sentence = String.Format("{0}\n\r Cette phrase se termine par un point.",final_sentence);
            else
                final_sentence = String.Format("{0}\n\r Cette phrase ne se termine pas par un point.",final_sentence);
            Console.WriteLine(final_sentence);
        }
    }
}
