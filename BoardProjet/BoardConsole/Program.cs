﻿using System;

namespace BoardConsole
{
    public class Program
    {
        public static int int_into_board(int [] tableau, int nb_search)
        {
            int indice=0;
            while(tableau[indice] != nb_search && indice < 9)
                indice++;
            if(indice == 9 && tableau[9] !=nb_search)
                indice =-2;
            indice++;
            return indice;
        } 
        static void Main(string[] args)
        {
            int[] tableau = new int [10];
            int valeur=0,indice, nb_search = 0;
            string valeur_str, phrase_saisie, nb_search_string, sentence;
            Console.WriteLine("Saisissez dix valeurs :");
            for(indice =0; indice<10 ; indice++)
            {
                do
                {
                    phrase_saisie = String.Format("{0} : ", indice + 1);
                    Console.Write(phrase_saisie);
                    valeur_str = Console.ReadLine();
                }
                while(!int.TryParse(valeur_str, out valeur));
                tableau[indice] = valeur;
            }
            Console.WriteLine("Saisissez une valeur : ");
            nb_search_string = Console.ReadLine();
            int.TryParse(nb_search_string, out nb_search);
            indice = int_into_board(tableau, nb_search);
            if(indice ==-1)
                sentence = String.Format("{0} est introuvable dans le tableau !", nb_search);
            else
                sentence = String.Format("{0} est la valeur n°{1}", nb_search, indice);
            Console.WriteLine(sentence);
        }
    }
}
