using System;
using Xunit;

using BoardConsole;

namespace BoardTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(2, Program.int_into_board(new int [10] {0,1,2,3,4,5,6,7,8,9},1));
            Assert.Equal(10, Program.int_into_board(new int [10] {0,1,2,3,4,5,6,7,8,9},9));
            Assert.Equal(-1, Program.int_into_board(new int [10] {0,1,2,3,4,5,6,7,8,9}, 12));
        }
    }
}
